output "websiteurl" {
  value = [aws_s3_bucket.cnscicd-1-bucket.bucket_domain_name
          ,aws_cloudfront_distribution.s3_distribution.domain_name]
}
