resource "aws_s3_bucket" "cnscicd-1-bucket" {
  bucket = "cnscicd-1-bucket"
  acl    = "private"

  tags = {
    Name = "cnscicd-1-bucket"
  }
}

resource "aws_s3_bucket_object" "cnscicd-1-bucket-folder" {
  key                    = "../src/"
  bucket                 = "${aws_s3_bucket.cnscicd-1-bucket.id}"
  source                 = "/dev/null"
  content_type           = "text/html"
}

resource "aws_s3_bucket_object" "cnscicd-1-bucket-file" {
  key                    = "../src/index.html"
  bucket                 = "${aws_s3_bucket.cnscicd-1-bucket.id}"
  source                 = "../src/index.html"
  content_type           = "text/html"
}

resource "aws_s3_bucket_policy" "cnscicd-1-bucket-policy" {
  bucket = "${aws_s3_bucket.cnscicd-1-bucket.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "Policy1584713872333",
    "Statement": [
        {
            "Sid": "Stmt1584713855644",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::cnscicd-1-bucket/*"
        }
     ]
}
POLICY
}
